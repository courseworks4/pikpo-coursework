DELETE FROM doctor;
DELETE FROM patient;
DELETE FROM schedule;
DELETE FROM doctor_schedule;

INSERT INTO patient (id, firstname, lastname) VALUES (1, 'Мирослава', 'Плотникова');
INSERT INTO patient (id, firstname, lastname) VALUES (2, 'Александра', 'Мельникова');
INSERT INTO patient (id, firstname, lastname) VALUES (3, 'Илья', 'Федоров');
INSERT INTO patient (id, firstname, lastname) VALUES (4, 'Виктория', 'Уткин');
INSERT INTO patient (id, firstname, lastname) VALUES (5, 'Никита', 'Плотникова');
INSERT INTO patient (id, firstname, lastname) VALUES (6, 'Илья', 'Сорокин');
INSERT INTO patient (id, firstname, lastname) VALUES (7, 'Василиса', 'Киреева');
INSERT INTO patient (id, firstname, lastname) VALUES (8, 'Марк', 'Леонтьев');
INSERT INTO patient (id, firstname, lastname) VALUES (9, 'Ксения', 'Зуева');
INSERT INTO patient (id, firstname, lastname) VALUES (10, 'Максим', 'Барсуков');

INSERT INTO doctor (id, firstname, lastname, specialization) VALUES (1, 'Руслан', 'Молчанов', 'хирург');
INSERT INTO doctor (id, firstname, lastname, specialization) VALUES (2, 'Егор', 'Егоров', 'хирург');
INSERT INTO doctor (id, firstname, lastname, specialization) VALUES (3, 'Константин', 'Орлов', 'хирург');
INSERT INTO doctor (id, firstname, lastname, specialization) VALUES (4, 'Борис', 'Трошин', 'дерматолог');
INSERT INTO doctor (id, firstname, lastname, specialization) VALUES (5, 'Максим', 'Мартынов', 'дерматолог');
INSERT INTO doctor (id, firstname, lastname, specialization) VALUES (6, 'Артём', 'Троицкий', 'вирусолог');;

INSERT INTO schedule (id, time) VALUES (1, '8:30');
INSERT INTO schedule (id, time) VALUES (2, '9:30');
INSERT INTO schedule (id, time) VALUES (3, '10:30');
INSERT INTO schedule (id, time) VALUES (4, '11:30');
INSERT INTO schedule (id, time) VALUES (5, '12:30');
INSERT INTO schedule (id, time) VALUES (6, '13:30');
INSERT INTO schedule (id, time) VALUES (7, '14:30');

INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (1, 1);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (1, 2);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (1, 3);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (1, 4);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (1, 5);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (1, 6);

INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (2, 1);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (2, 2);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (2, 3);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (2, 4);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (2, 5);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (2, 6);

INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (3, 1);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (3, 2);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (3, 3);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (3, 5);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (3, 6);

INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (4, 1);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (4, 2);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (4, 5);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (4, 6);

INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (5, 3);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (5, 4);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (5, 5);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (5, 6);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (5, 7);


INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (6, 1);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (6, 2);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (6, 3);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (6, 4);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (6, 5);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (6, 6);
INSERT INTO doctor_schedule (schedule_id, doctor_id) VALUES (6, 7);

INSERT INTO role (id, name) VALUES (1, 'ROLE_USER');
INSERT INTO role (id, name) VALUES (2, 'ROLE_ADMIN');
INSERT INTO registrar (id, password, username) VALUES (1, '$2a$10$yygjUhUpY56itp6z/oNqOOwHttVo7v17EnL4JYtk7lvjJDEzSsD12', 'admin');
INSERT INTO registrar (id, password, username) VALUES (2, '$2a$10$dzfM/613QhMtNdTT5WutLesXTM3KyHKpx8oU5Rnlqitc3QkaKXpmq', 'user');

INSERT INTO registrar_roles (users_id, roles_id) VALUES (1, 1);
INSERT INTO registrar_roles (users_id, roles_id) VALUES (1, 2);
INSERT INTO registrar_roles (users_id, roles_id) VALUES (2, 1);