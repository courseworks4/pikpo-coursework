package com.example.coursework.utils;

import com.example.coursework.model.dao.Doctor;
import com.example.coursework.model.dao.Schedule;
import com.example.coursework.service.DoctorService;
import com.example.coursework.service.RegistrarService;
import com.example.coursework.service.ScheduleService;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Lazy(value = false)
public class InitializeDoctors {
    private final DoctorService doctorService;
    private final ScheduleService scheduleService;

    @Autowired
    public InitializeDoctors(DoctorService doctorService,
                             ScheduleService scheduleService) {
        this.doctorService = doctorService;
        this.scheduleService = scheduleService;
    }

    //@PostConstruct
    public void load() {
        doctorService.deleteAll();
        scheduleService.deleteAll();
        loadDoctors();
    }

    public void loadDoctors() {
        Schedule schedule1 = new Schedule("17:30");
        Schedule schedule2 = new Schedule("12:30");
        Schedule schedule3 = new Schedule("13:30");
        Schedule schedule4 = new Schedule("14:30");
        Schedule schedule5 = new Schedule("15:30");
        Schedule schedule6 = new Schedule("16:30");

        Doctor first = Doctor.builder()
                .id(1L).firstname("Василий").lastname("Баранов").specialization("хирург").build();
        Doctor second = Doctor.builder()
                .id(2L).firstname("Виталий").lastname("Корнев").specialization("терапевт").build();
        Doctor third = Doctor.builder()
                .id(3L).firstname("Данил").lastname("Киркин").specialization("хирург").build();
        Doctor fourth = Doctor.builder()
                .id(4L).firstname("Александр").lastname("Иванов").specialization("дерматолог").build();
        Doctor fifth = Doctor.builder()
                .id(5L).firstname("Владимир").lastname("Булгаков").specialization("терапевт").build();

        schedule1.setDoctors(new ArrayList<>(List.of(first, second, third, fifth)));
        schedule2.setDoctors(new ArrayList<>(List.of(first, second, third, fifth)));
        schedule4.setDoctors(new ArrayList<>(List.of(first, second, fourth, fifth)));
        schedule3.setDoctors(new ArrayList<>(List.of(first, second, third, fifth)));
        schedule5.setDoctors(new ArrayList<>(List.of(fifth)));
        schedule6.setDoctors(new ArrayList<>(List.of(fifth)));

        first.setSchedules(new ArrayList<>(List.of(schedule1, schedule2, schedule3, schedule4)));
        second.setSchedules(new ArrayList<>(List.of(schedule1, schedule2, schedule3, schedule4)));
        third.setSchedules(new ArrayList<>(List.of(schedule1, schedule2, schedule3)));
        fourth.setSchedules(new ArrayList<>(List.of(schedule4)));
        fifth.setSchedules(new ArrayList<>(List.of(schedule1, schedule2, schedule3, schedule4, schedule5, schedule6)));

        doctorService.saveAll(List.of(
                first, second, third, fourth, fifth
        ));
        scheduleService.saveAll(List.of(
                schedule1, schedule2, schedule3, schedule4, schedule5, schedule6)
        );
    }
}
