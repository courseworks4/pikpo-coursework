package com.example.coursework.utils;

public enum Page {
    APPOINTMENT_FORM("appointment.html"),
    APPOINTMENTS("appointments.html"),
    DOCTORS("doctors.html"),
    HOME("homePage.html"),
    PATIENT_APPOINTMENTS("patientAppForm.html"),
    PATIENTS("patients.html"),
    REGISTRATION("registration.html"),
    REGISTRARS("registrators.html"),
    USER_FORM("userForm.html");

    public final String page;

    Page(String page) {
        this.page = page;
    }
}
