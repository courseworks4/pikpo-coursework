package com.example.coursework.utils;

import com.example.coursework.model.dao.Patient;
import com.example.coursework.service.PatientService;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class InitializePatients {
    private final PatientService patientService;

    @Autowired
    public InitializePatients(PatientService patientService) {
        this.patientService = patientService;
    }

    //@PostConstruct
    public void init(){
        patientService.deleteAll();
        load();
    }

    public void load(){
        Patient patient = Patient.builder().id(1L).firstname("Данил").lastname("Мокшанцев").build();
        Patient patient1 = Patient.builder().id(2L).firstname("Павел").lastname("Колесников").build();
        Patient patient2 = Patient.builder().id(3L).firstname("Владимир").lastname("Колпаков").build();
        Patient patient3 = Patient.builder().id(4L).firstname("Михаил").lastname("Звубенко").build();
        patientService.saveAll(new ArrayList<>(List.of(patient1, patient2, patient3, patient)));
    }
}
