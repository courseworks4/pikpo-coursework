package com.example.coursework.controller;

import com.example.coursework.model.dao.Doctor;
import com.example.coursework.model.dao.Patient;
import com.example.coursework.model.dao.Registrar;
import com.example.coursework.service.AppointmentsService;
import com.example.coursework.service.DoctorService;
import com.example.coursework.service.PatientService;
import com.example.coursework.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

//todo логирование
@Controller
@RequestMapping("/mvc/v1")
public class AppointmentsController {
    private final DoctorService doctorService;
    private final PatientService patientService;
    private final AppointmentsService appointmentsService;

    @Autowired
    public AppointmentsController(DoctorService doctorService,
                                  PatientService patientService,
                                  AppointmentsService appointmentsService) {
        this.doctorService = doctorService;
        this.patientService = patientService;
        this.appointmentsService = appointmentsService;
    }

    @GetMapping("/addAppointment")
    public String appointmentForm(Model model,
                                  @RequestParam(required = false, name = "patient_id") Long id,
                                  @RequestParam(required = false) String specialization,
                                  @RequestParam(required = false, name = "doctor-id") Long doctorId,
                                  @RequestParam(required = false) String date,
                                  @RequestParam(required = false) String time) {
        model.addAttribute("specializations", doctorService.getAllSpecialization());
        model.addAttribute("message", "Введите идентификатор пользователя");
        if (id != null) {
            model.addAttribute("id", id);
            if (patientService.hasPatient(id)) {
                Patient patient = patientService.getById(id);
                model.addAttribute(patient);
            }
        }
        //todo отправлять в модель available schedule
        if (specialization != null && !specialization.isEmpty()) {
            model.addAttribute("doctors", doctorService.getAllBySpecialization(specialization));
            model.addAttribute("specialization", specialization);
        }
        if (doctorId != null) {
            Doctor doctor = doctorService.getById(doctorId);
            if (doctor.getSpecialization().equals(specialization)) {
                model.addAttribute("doctor", doctor);
                if (date != null && !date.isEmpty()) {
                    model.addAttribute("schedule", doctorService.getAvailableSchedule(doctor, date));
                    model.addAttribute("date", date);
                }
                model.addAttribute("time", time);
            }
        }

        return Page.APPOINTMENT_FORM.page;
    }

    @PostMapping("/addAppointment")
    public String save(@RequestParam(required = false, name = "patient_id") Long id,
                       @RequestParam(name = "doctor-id") Long doctorId,
                       @RequestParam String date,
                       @RequestParam String time) {
        appointmentsService.saveByInfo(
                patientService.getById(id), doctorService.getById(doctorId), date, time);
        return "redirect:/mvc/v1/main";
    }

    @GetMapping("/appointments")
    public String getAll(Model model,
                         @RequestParam(required = false, name = "patient_id") Long patientId,
                         @RequestParam(required = false, name = "doctor_id") Long doctorId) {
        if (patientId == null && doctorId == null)
            model.addAttribute("list", appointmentsService.getAll());
        else {
            model.addAttribute("list", appointmentsService.getAllByDoctorAndPatient(doctorId, patientId));
        }
        return Page.APPOINTMENTS.page;
    }

    @GetMapping("/patient/appointments")
    public String getPatientAppointmentsForm(
            Model model,
            @RequestParam(required = false, name = "patient_id") Long id
    ){
        if(id != null) {
            model.addAttribute("list", appointmentsService.getAllByPatientId(id));
            model.addAttribute("id", id);
        }
        return Page.PATIENT_APPOINTMENTS.page;
    }

    //Почему-то не видит метод DELETE из thymeleaf
    @PostMapping("/appointments")
    public String deleteAppointment(@RequestParam Long id) {
        appointmentsService.deleteById(id);
        return "redirect:/mvc/v1/main";
    }
}
