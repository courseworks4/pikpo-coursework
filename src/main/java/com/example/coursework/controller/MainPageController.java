package com.example.coursework.controller;

import com.example.coursework.model.dao.Registrar;
import com.example.coursework.model.dao.Role;
import com.example.coursework.utils.Page;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller()
@RequestMapping("/mvc/v1")
public class MainPageController {
    @GetMapping("/main")
    public String mainPage(Model model, @AuthenticationPrincipal Registrar registrar) {
        model.addAttribute("isAdmin", registrar.getAuthorities().contains(new Role("ROLE_ADMIN")));
        model.addAttribute("username", registrar.getUsername());
        return Page.HOME.page;
    }
}
