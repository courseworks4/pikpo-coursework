package com.example.coursework.controller;

import com.example.coursework.model.dto.Registration;
import com.example.coursework.model.dto.UserResponse;
import com.example.coursework.service.*;
import com.example.coursework.utils.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/admin")
public class AdminController {
    private final RegistrarService registrarService;

    private HashMap<String, MyService> map = new HashMap<>();

    public AdminController(DoctorService doctorService,
                           PatientService patientService,
                           RegistrarService registrarService) {
        this.registrarService = registrarService;
        map.put("doctors", doctorService);
        map.put("patients", patientService);
    }

    @GetMapping("/all")
    public String getAllAppointments(Model model, @RequestParam String attribute) {
        if(map.containsKey(attribute))
            model.addAttribute("list", map.get(attribute).getAll());
        else
            return "homePage.html";
        return Page.valueOf(attribute.toUpperCase()).page;
    }

    @GetMapping("/registrar/add")
    public String registrarRegistrationForm(){
        return Page.REGISTRATION.page;
    }
    @PostMapping("/registrar/add")
    public String addRegistrar(@ModelAttribute Registration registration){
        registrarService.createRegistrar(registration.getLogin(), registration.getPassword());
        return "redirect:/mvc/v1/main";
    }
    @GetMapping("/registrar/all")
    public String getAllUsers(Model model){
        model.addAttribute("users", registrarService.getAll());
        return Page.REGISTRARS.page;
    }

    @GetMapping("/registrar")
    public String modifyUserForm(Model model, @RequestParam Long id){
        model.addAttribute("user", registrarService.getById(id));
        return Page.USER_FORM.page;
    }

    @PostMapping("/modify-registrar")
    public String modifyUser(@RequestParam Long id, @RequestParam String password){
        registrarService.updatePassword(id, password);
        return "redirect:/admin/registrar/all";
    }
}
