package com.example.coursework.service;

import com.example.coursework.model.dao.Patient;
import com.example.coursework.repository.PatientRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientService implements MyService<Patient>{
    private final PatientRepository patientRepository;

    @Override
    public List<Patient> getAll() {
        return patientRepository.findAll();
    }

    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public void save(Patient patient){
        patientRepository.save(patient);
    }

    public void saveAll(List<Patient> patients){
        patientRepository.saveAll(patients);
    }

    //todo
    public Patient getById(Long id){
        return patientRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    public boolean hasPatient(Long id){
        return patientRepository.existsById(id);
    }

    public void deleteAll(){
        patientRepository.deleteAll();
    }
}
