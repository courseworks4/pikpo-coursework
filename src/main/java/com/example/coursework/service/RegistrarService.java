package com.example.coursework.service;

import com.example.coursework.model.dao.Registrar;
import com.example.coursework.model.dao.Role;
import com.example.coursework.model.dto.UserResponse;
import com.example.coursework.repository.RegistrarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class RegistrarService implements UserDetailsService {
    private final RegistrarRepository registrarRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public RegistrarService(RegistrarRepository registrarRepository) {
        passwordEncoder = new BCryptPasswordEncoder(10);
        this.registrarRepository = registrarRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return registrarRepository.findByUsername(username);

    }

    public List<UserResponse> getAll(){
        return registrarRepository.findAll().stream().map(e -> {
            UserResponse userResponse = new UserResponse();
            userResponse.setId(e.getId());
            userResponse.setLogin(e.getUsername());
            userResponse.setRoles(e.getRoles());
            return userResponse;
        }).collect(Collectors.toList());
    }

    public UserResponse getById(Long id){
        Registrar e = registrarRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        UserResponse userResponse = new UserResponse();
        userResponse.setId(e.getId());
        userResponse.setLogin(e.getUsername());
        userResponse.setRoles(e.getRoles());
        return userResponse;
    }

    public void createRegistrar(String username, String password){
        Registrar registrar = new Registrar();
        registrar.setUsername(username);
        registrar.setPassword(passwordEncoder.encode(password));
        registrar.setRoles(Set.of(new Role(1L, "ROLE_USER")));
        registrarRepository.save(registrar);
    }

    public void updatePassword(Long id, String password){
        Registrar registrar = registrarRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        registrar.setPassword(passwordEncoder.encode(password));
        registrarRepository.save(registrar);
    }
}
