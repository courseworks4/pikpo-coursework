package com.example.coursework.service;

import com.example.coursework.model.dao.Appointment;
import com.example.coursework.model.dao.Doctor;
import com.example.coursework.model.dao.Patient;
import com.example.coursework.repository.AppointmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AppointmentsService implements MyService<Appointment> {
    private final AppointmentRepository appointmentRepository;

    @Autowired
    public AppointmentsService(AppointmentRepository appointmentRepository) {
        this.appointmentRepository = appointmentRepository;
    }

    public void save(Appointment appointment){
        appointmentRepository.save(appointment);
    }

    public List<Appointment> getAll(){
        return appointmentRepository.findAll();
    }

    public List<Appointment> getAllByPatientId(Long patientId){
        return appointmentRepository.findAllByPatientId(patientId);
    }

    public List<Appointment> getAllByDoctorId(Long doctorId){
        return appointmentRepository.findAllByDoctorId(doctorId);
    }

    public void saveByInfo(Patient patient, Doctor doctor, String date, String time){
        Appointment appointment = Appointment.builder()
                .doctor(doctor)
                .patient(patient)
                .appointmentDate(LocalDate.parse(date))
                .time(time).build();
        appointmentRepository.save(appointment);
    }

    public List<Appointment> getAllByDoctorAndPatient(Long doctor, Long patient){
        if(doctor == null){
            return appointmentRepository.findAllByPatientId(patient);
        }
        if(patient == null){
            return appointmentRepository.findAllByDoctorId(doctor);
        }
        List<Appointment> allByPatientId = appointmentRepository.findAllByPatientId(patient);
        Set<Long> set = allByPatientId.stream().map(Appointment::getId).collect(Collectors.toSet());
        List<Appointment> result = new ArrayList<>();
        for (Appointment a : appointmentRepository.findAllByDoctorId(doctor)) {
            if(set.contains(a.getId())){
                result.add(a);
            }
        }
        return result;
    }

    public void deleteById(Long id){
        appointmentRepository.deleteById(id);
    }

    public List<Appointment> getAppointmentByDate(LocalDate date){
        return appointmentRepository.findAllByDate(date);
    }
}
