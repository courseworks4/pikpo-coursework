package com.example.coursework.service;

import java.util.List;

public interface MyService<T> {
    public List<T> getAll();
}
