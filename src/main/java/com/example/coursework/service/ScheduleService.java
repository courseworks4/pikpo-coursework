package com.example.coursework.service;

import com.example.coursework.model.dao.Doctor;
import com.example.coursework.model.dao.Schedule;
import com.example.coursework.repository.ScheduleRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ScheduleService {
    private final ScheduleRepository scheduleRepository;

    public ScheduleService(ScheduleRepository scheduleRepository) {
        this.scheduleRepository = scheduleRepository;
    }

    public void save(Schedule schedule){
        scheduleRepository.save(schedule);
    }

    public void saveAll(List<Schedule> schedules){
        scheduleRepository.saveAll(schedules);
    }

    public void deleteAll(){
        scheduleRepository.deleteAll();
    }


}
