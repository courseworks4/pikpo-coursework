package com.example.coursework.service;

import com.example.coursework.model.dao.Appointment;
import com.example.coursework.model.dao.Doctor;
import com.example.coursework.model.dao.Schedule;
import com.example.coursework.repository.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class DoctorService implements MyService<Doctor>{
    private final DoctorRepository doctorRepository;
    private final AppointmentsService appointmentsService;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository,
                         AppointmentsService appointmentsService) {
        this.appointmentsService = appointmentsService;
        this.doctorRepository = doctorRepository;
    }

    public void save(Doctor doctor){
        doctorRepository.save(doctor);
    }

    public List<Doctor> getAll(){
        return doctorRepository.findAll();
    }

    public List<Doctor> getAllBySpecialization(String specialization){
        return doctorRepository.findAllBySpecialization(specialization);
    }
    //todo to dto
    public Doctor getById(Long id){
        return doctorRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }
    public Set<String> getAllSpecialization(){
        return doctorRepository.findAll().stream().map(Doctor::getSpecialization).collect(Collectors.toSet());
    }

    public void saveAll(List<Doctor> doctors){
        doctorRepository.saveAll(doctors);
    }

    public void deleteAll(){
        doctorRepository.deleteAll();
    }

    public List<String> getAvailableSchedule(Doctor doctor, String date){
        List<String> doctorSchedule = doctor.getSchedules().stream().map(Schedule::getTime).toList();
        Set<String> busyTime = appointmentsService
                .getAppointmentByDate(LocalDate.parse(date)).stream().map(Appointment::getTime).collect(Collectors.toSet());
        List<String> availableTime = new ArrayList<>();
        for (String s : doctorSchedule) {
            if(!busyTime.contains(s)) availableTime.add(s);
        }
        return availableTime;
    }
}
