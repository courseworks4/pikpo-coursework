package com.example.coursework.repository;

import com.example.coursework.model.dao.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Long> {
    @Query(value = "select * from doctor where specialization = :specialization", nativeQuery = true)
    List<Doctor> findAllBySpecialization(String specialization);
}
