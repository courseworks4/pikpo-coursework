package com.example.coursework.repository;

import com.example.coursework.model.dao.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long> {
    List<Appointment> findAllByPatientId(Long id);

    List<Appointment> findAllByDoctorId(Long doctorId);

    @Query(value = "select a from Appointment a where a.appointmentDate = :date")
    List<Appointment> findAllByDate(LocalDate date);

}
