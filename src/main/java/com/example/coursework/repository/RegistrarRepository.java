package com.example.coursework.repository;

import com.example.coursework.model.dao.Registrar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RegistrarRepository extends JpaRepository<Registrar, Long> {
    //@Query("select Registrar from Registrar r where r.username = :username")
    Registrar findByUsername(String username);
}
