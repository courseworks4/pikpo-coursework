package com.example.coursework.model.dao;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "patient")
@Data
@SuperBuilder
@NoArgsConstructor
public class Patient {
    @Id
    private Long id;
    private String firstname;
    private String lastname;
}
