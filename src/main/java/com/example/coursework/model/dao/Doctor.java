package com.example.coursework.model.dao;

import jakarta.persistence.*;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Entity
@Table(name = "doctor")
@SuperBuilder
@Data
@NoArgsConstructor
public class Doctor {
    @Id
    private Long id;
    private String firstname;
    private String lastname;
    private String specialization;
    @ManyToMany(mappedBy = "doctors")
    private List<Schedule> schedules;

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", specialization='" + specialization + '\'' +
                '}';
    }
}
