package com.example.coursework.model.dto;

import com.example.coursework.model.dao.Role;
import lombok.Data;

import java.util.Collection;

@Data
public class UserResponse {
    private Long id;
    private String login;
    private Collection<Role> roles;
}
