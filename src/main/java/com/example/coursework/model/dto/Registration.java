package com.example.coursework.model.dto;

import lombok.Data;

@Data
public class Registration {
    private String login;
    private String password;
}
